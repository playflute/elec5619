
package com.csvimport.service;

import java.util.List;

import com.csvimport.model.ScoreData;
import com.csvimport.model.SleepStatsData;
import com.csvimport.model.User;


public interface ScoreDataService {

    ScoreData save(ScoreData scoreData);

    boolean findById(int id);
    List<ScoreData> findByUser(User user);

    //boolean findByUser(User user);
}
