
package com.csvimport.service;

import com.csvimport.model.Csvdata;
import com.csvimport.model.User;
import java.util.Date;
import java.util.List;

public interface CsvdataService {

    Csvdata save(Csvdata csvdata);

    boolean findById(int id);

    List<Csvdata> findByUser(User user,Date rdate);
    Csvdata findCsvdataById(Integer id);
 	public List<Csvdata> getAllCsvdata();
 	
 	public List<Csvdata> list();
 	
 	
 	public void delCsvdata(Long id);
 	
 	public void updateCsvdata(Csvdata csvdata);
}
