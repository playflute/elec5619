package com.csvimport.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.csvimport.model.SleepStatsData;
import com.csvimport.model.User;
import com.csvimport.repository.SleepStatsDataRepository;
@Service ("sleepStatsDataService")
public class SleepStatsDataServiceImpl implements SleepStatsDataService {

	@Autowired
	private SleepStatsDataRepository sleepStatsDataRepository;
	@Transactional
	public SleepStatsData save(SleepStatsData sleepStatsData) {
		return sleepStatsDataRepository.save(sleepStatsData);
	}

	public boolean findById(int id) {
		//return sleepStatsDataRepository.findById(id);
		return true;
	}
	public List<SleepStatsData> findByUser(User user) {
		return sleepStatsDataRepository.findByUser(user);
	}
}
