package com.csvimport.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.csvimport.model.ScoreData;
import com.csvimport.model.SleepStatsData;
import com.csvimport.model.User;
import com.csvimport.repository.ScoreDataRepository;
import com.csvimport.repository.SleepStatsDataRepository;
@Service ("scoreDataService")
public class ScoreDataServiceImpl implements ScoreDataService {

	@Autowired
	private ScoreDataRepository scoreDataRepository;
	
	@Transactional
	public ScoreData save(ScoreData scoreData) {
		// TODO Auto-generated method stub
		return scoreDataRepository.saveAndFlush(scoreData);
	}

	public boolean findById(int id) {
		//return sleepStatsDataRepository.findById(id);
		return true;
	}
	public List<ScoreData> findByUser(User user) {
		return scoreDataRepository.findByUser(user);
	}

	
}
