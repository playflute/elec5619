
package com.csvimport.service;

import com.csvimport.model.Csvdata;
import com.csvimport.model.User;
import com.csvimport.repository.CsvdataRepository;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service("csvdataService")
public class CsvdataServiceImpl implements CsvdataService {

    @Autowired
    private CsvdataRepository csvdataRepository;

    @Transactional
    public Csvdata save(Csvdata csvdata) {
        return csvdataRepository.save(csvdata);
    }

    public boolean findById(int id) {
        return true;
    }

    public List<Csvdata> findByUser(User user,Date rdate) {
        return csvdataRepository.findByUser(user,rdate);
    }
    public Csvdata findCsvdataById(Integer id) {
		return csvdataRepository.findCsv(id);
	}


	public List<Csvdata> getAllCsvdata() {
		return csvdataRepository.findAll();
	}


	public List<Csvdata> list() {
		return csvdataRepository.findAll();
	}


	public void delCsvdata(Long id) {
		Csvdata sd=findCsvdataById(id.intValue());
		 csvdataRepository.delete(sd);
	}


	public void updateCsvdata(Csvdata csvdata) {
		 csvdataRepository.save(csvdata);
	}
}
