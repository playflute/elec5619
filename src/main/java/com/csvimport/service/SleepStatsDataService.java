
package com.csvimport.service;

import java.util.List;

import com.csvimport.model.SleepStatsData;
import com.csvimport.model.User;


public interface SleepStatsDataService {

    SleepStatsData save(SleepStatsData sleepStatsData);

    boolean findById(int id);
    List<SleepStatsData> findByUser(User user);

    //boolean findByUser(User user);
}
