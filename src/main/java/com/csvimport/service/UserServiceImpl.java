
package com.csvimport.service;

import com.csvimport.model.User;
import com.csvimport.repository.UserRepository;
import java.util.List;
import javax.persistence.Query;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("userService")
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Transactional
    public User save(User user) {
        return userRepository.save(user);
    }

    public boolean findByLogin(String userName, String password) {
        User user = userRepository.findByUserName(userName);

        if (user != null && user.getPassword().equals(password)) {
            return true;
        }

        return false;
    }

    public boolean findByUserName(String userName) {
        User user = userRepository.findByUserName(userName);

        if (user != null) {
            return true;
        }

        return false;
    }
    
    public List<User> findAll() {
        List<User> userList = userRepository.findAll();

        
        return userList;
    }

    public User findByUser(String userName) {
        return userRepository.findByUserName(userName);
    }
}
