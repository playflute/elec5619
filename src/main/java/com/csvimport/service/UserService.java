
package com.csvimport.service;

import com.csvimport.model.User;
import java.util.List;

public interface UserService {

    User save(User user);

    boolean findByLogin(String userName, String password);

    boolean findByUserName(String userName);
    
    User findByUser(String userName);
    
    List<User> findAll();
}
