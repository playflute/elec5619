
package com.csvimport.model;

import java.util.Date;

public class ChartData {

    private String userID1;
    private String userID2;
    private Date chartDate;

    public String getUserID1() {
        return userID1;
    }

    public void setUserID1(String userID1) {
        this.userID1 = userID1;
    }

    public String getUserID2() {
        return userID2;
    }

    public void setUserID2(String userID2) {
        this.userID2 = userID2;
    }

   

    public Date getChartDate() {
        return chartDate;
    }

    public void setChartDate(Date chartDate) {
        this.chartDate = chartDate;
    }

}
