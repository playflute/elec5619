
package com.csvimport.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "csvdata")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Csvdata.findAll", query = "SELECT c FROM Csvdata c"),
    @NamedQuery(name = "Csvdata.findById", query = "SELECT c FROM Csvdata c WHERE c.id = :id"),
    @NamedQuery(name = "Csvdata.findByRdate", query = "SELECT c FROM Csvdata c WHERE c.rdate = :rdate"),
    @NamedQuery(name = "Csvdata.findByRtime", query = "SELECT c FROM Csvdata c WHERE c.rtime = :rtime"),
    @NamedQuery(name = "Csvdata.findByXAxisMean", query = "SELECT c FROM Csvdata c WHERE c.xAxisMean = :xAxisMean"),
    @NamedQuery(name = "Csvdata.findByYAxisMean", query = "SELECT c FROM Csvdata c WHERE c.yAxisMean = :yAxisMean"),
    @NamedQuery(name = "Csvdata.findByZAxisMean", query = "SELECT c FROM Csvdata c WHERE c.zAxisMean = :zAxisMean"),
    @NamedQuery(name = "Csvdata.findByLux", query = "SELECT c FROM Csvdata c WHERE c.lux = :lux"),
    @NamedQuery(name = "Csvdata.findByButton", query = "SELECT c FROM Csvdata c WHERE c.button = :button"),
    @NamedQuery(name = "Csvdata.findByTemp", query = "SELECT c FROM Csvdata c WHERE c.temp = :temp"),
    @NamedQuery(name = "Csvdata.findBySvmgs", query = "SELECT c FROM Csvdata c WHERE c.svmgs = :svmgs"),
    @NamedQuery(name = "Csvdata.findByXAxisStdev", query = "SELECT c FROM Csvdata c WHERE c.xAxisStdev = :xAxisStdev"),
    @NamedQuery(name = "Csvdata.findByYAxisStdev", query = "SELECT c FROM Csvdata c WHERE c.yAxisStdev = :yAxisStdev"),
    @NamedQuery(name = "Csvdata.findByZAxisStdev", query = "SELECT c FROM Csvdata c WHERE c.zAxisStdev = :zAxisStdev"),
    @NamedQuery(name = "Csvdata.findByLight", query = "SELECT c FROM Csvdata c WHERE c.light = :light"),
    @NamedQuery(name = "Csvdata.findByActivity", query = "SELECT c FROM Csvdata c WHERE c.activity = :activity")})
public class Csvdata implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "rdate")
    @Temporal(TemporalType.DATE)
    private Date rdate;
    @Column(name = "rtime")
    @Temporal(TemporalType.TIME)
    private Date rtime;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "x_axis_mean")
    private Double xAxisMean;
    @Column(name = "y_axis_mean")
    private Double yAxisMean;
    @Column(name = "z_axis_mean")
    private Double zAxisMean;
    @Column(name = "lux")
    private Integer lux;
    @Column(name = "button")
    private Integer button;
    @Column(name = "temp")
    private Double temp;
    @Column(name = "svmgs")
    private Integer svmgs;
    @Column(name = "x_axis_stdev")
    private Integer xAxisStdev;
    @Column(name = "y_axis_stdev")
    private Integer yAxisStdev;
    @Column(name = "z_axis_stdev")
    private Integer zAxisStdev;
    @Column(name = "light")
    private Integer light;
    @Column(name = "activity")
    private Double activity;
    @JoinColumn(name = "userid", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private User userid;

    public Csvdata() {
    }

    public Csvdata(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getRdate() {
        return rdate;
    }

    public void setRdate(Date rdate) {
        this.rdate = rdate;
    }

    public Date getRtime() {
        return rtime;
    }

    public void setRtime(Date rtime) {
        this.rtime = rtime;
    }

    public Double getXAxisMean() {
        return xAxisMean;
    }

    public void setXAxisMean(Double xAxisMean) {
        this.xAxisMean = xAxisMean;
    }

    public Double getYAxisMean() {
        return yAxisMean;
    }

    public void setYAxisMean(Double yAxisMean) {
        this.yAxisMean = yAxisMean;
    }

    public Double getZAxisMean() {
        return zAxisMean;
    }

    public void setZAxisMean(Double zAxisMean) {
        this.zAxisMean = zAxisMean;
    }

    public Integer getLux() {
        return lux;
    }

    public void setLux(Integer lux) {
        this.lux = lux;
    }

    public Integer getButton() {
        return button;
    }

    public void setButton(Integer button) {
        this.button = button;
    }

    public Double getTemp() {
        return temp;
    }

    public void setTemp(Double temp) {
        this.temp = temp;
    }

    public Integer getSvmgs() {
        return svmgs;
    }

    public void setSvmgs(Integer svmgs) {
        this.svmgs = svmgs;
    }

    public Integer getXAxisStdev() {
        return xAxisStdev;
    }

    public void setXAxisStdev(Integer xAxisStdev) {
        this.xAxisStdev = xAxisStdev;
    }

    public Integer getYAxisStdev() {
        return yAxisStdev;
    }

    public void setYAxisStdev(Integer yAxisStdev) {
        this.yAxisStdev = yAxisStdev;
    }

    public Integer getZAxisStdev() {
        return zAxisStdev;
    }

    public void setZAxisStdev(Integer zAxisStdev) {
        this.zAxisStdev = zAxisStdev;
    }

    public Integer getLight() {
        return light;
    }

    public void setLight(Integer light) {
        this.light = light;
    }

    public Double getActivity() {
        return activity;
    }

    public void setActivity(Double activity) {
        this.activity = activity;
    }

    public User getUserid() {
        return userid;
    }

    public void setUserid(User userid) {
        this.userid = userid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Csvdata)) {
            return false;
        }
        Csvdata other = (Csvdata) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.csvimport.model.Csvdata[ id=" + id + " ]";
    }
    
}
