package com.csvimport.controller;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.csvimport.model.Csvdata;
import com.csvimport.model.User;
import com.csvimport.service.CsvdataService;

@Controller
//@RequestMapping("/chart")
public class ChartImgController {
	
	 @Autowired
	 private CsvdataService csvdataService;
	 private String temprect;
	@Autowired
	private  HttpServletRequest request;
	SimpleDateFormat s=new SimpleDateFormat("yyyy-MM-dd,HH:mm:ss");
	SimpleDateFormat s1=new SimpleDateFormat("yyyy,MM,dd");
	SimpleDateFormat s2=new SimpleDateFormat("HH,mm,ss");
	@RequestMapping("/getAllChart")
	public String getAllChart(HttpServletRequest request,HttpServletResponse response){
		List<Csvdata> chartlist=csvdataService.getAllCsvdata();
		String path = null;
		System.out.println(getPorjectPath());
		if(request.getParameter("tempactivie")==null||request.getParameter("tempactivie").equals("")||request.getParameter("tempactivie").equals("1")){
			path="D:\\.metadata\\.plugins\\org.eclipse.wst.server.core\\tmp0\\wtpwebapps\\a\\resources\\js\\data.tsv";
			
		}else{
			path="D:\\.metadata\\.plugins\\org.eclipse.wst.server.core\\tmp0\\wtpwebapps\\a\\resources\\js\\data1.tsv";
		}
	 	
	 	path=path.replace("bin", "webapps");
	 	
	 	System.out.println(path.toString());
	 	File file=new File(path);
	 	try {
			file.createNewFile();
		} catch (IOException e) {
			e.printStackTrace();
		}
		StringBuffer sb=new StringBuffer();
		List<String> dlist=new ArrayList<String>();
		temprect="";
		String startdate="";
		String enddate="";
		sb.append("date	close").append("\r\n");
			if(request.getParameter("tempactivie")==null||request.getParameter("tempactivie").equals("")||request.getParameter("tempactivie").equals("1")){
				for(Csvdata ct:chartlist){
					sb.append(ct.getRdate()).append(" ").append(ct.getRtime()).append("	  ").append(ct.getTemp()).append("\r\n");
					request.setAttribute("tempactivie", 1);
					if(ct.getButton()==1&&startdate.equals("")){
						String aa1[]=s1.format(ct.getRdate()).split(",");
						startdate=aa1[0]+","+(Integer.valueOf(aa1[1])-1)+","+aa1[2]+","+s2.format(ct.getRtime())+",0";
					}else if(!startdate.equals("")&&ct.getButton()==0){
						String aa1[]=s1.format(ct.getRdate()).split(",");
						enddate=aa1[0]+","+(Integer.valueOf(aa1[1])-1)+","+aa1[2]+","+s2.format(ct.getRtime())+",0";
						dlist.add(startdate+"#"+enddate);
						startdate="";enddate="";
					}
				}
				bufferwriterToFile(sb.toString(), path);
				for(String sd:dlist){
					String[] sf=sd.split("#");
					temprect+=" var width_interval = x(new Date("+sf[1]+")) - x(new Date("+sf[0]+"));"+
            "svg.append(\"rect\")"+
                ".attr(\"x\", function (d) { return x(new Date("+sf[0]+")); })"+
                ".attr(\"width\",width_interval)"+
                ".attr(\"height\", height)\r\n";
				}
				request.setAttribute("temprect", temprect);
				return "/d3";
			}else if(request.getParameter("tempactivie").equals("2")){
				for(Csvdata ct:chartlist){
					sb.append(ct.getRdate()).append(" ").append(ct.getRtime()).append("	").append(ct.getActivity()).append("\r\n");
					request.setAttribute("tempactivie", 2);
					if(ct.getButton()==1&&startdate.equals("")){
						String aa1[]=s1.format(ct.getRdate()).split(",");
						startdate=aa1[0]+","+(Integer.valueOf(aa1[1])-1)+","+aa1[2]+","+s2.format(ct.getRtime())+",0";
					}else if(!startdate.equals("")&&ct.getButton()==0){
						String aa1[]=s1.format(ct.getRdate()).split(",");
						enddate=aa1[0]+","+(Integer.valueOf(aa1[1])-1)+","+aa1[2]+","+s2.format(ct.getRtime())+",0";
						dlist.add(startdate+"#"+enddate);
						startdate="";enddate="";
					}
				}
				bufferwriterToFile(sb.toString(), path);
				for(String sd:dlist){
					String[] sf=sd.split("#");
					temprect+=" var width_interval = x(new Date("+sf[1]+")) - x(new Date("+sf[0]+"));"+
            "svg.append(\"rect\")"+
                ".attr(\"x\", function (d) { return x(new Date("+sf[0]+")); })"+
                ".attr(\"width\",width_interval)"+
                ".attr(\"height\", height);\r\n";
				}
				request.setAttribute("temprect", temprect);
				return "/d4";
			}
			return path;
	}
	@RequestMapping("/list")
	public String login(HttpServletRequest request){
		request.setAttribute("chartList", csvdataService.getAllCsvdata());
		
		return "/chartmainpage";
	}
	@RequestMapping("/index")
	public String list(HttpServletRequest request){
		return "login";
		
	}
	private String projectName="UploadCSVMavenProject";        //  你项目的名称

	//获取当前项目的绝对路径
	  public String getPorjectPath(){
	   String nowpath;             
	   String tempdir;
	   nowpath=System.getProperty("user.dir");
	   tempdir=nowpath.replace("bin", "webapps");  //把bin 文件夹变到 webapps文件里面 
	   tempdir+="//"+projectName;  
	   return tempdir;  
	  }
	@RequestMapping("/getChart")
	public String getChart(String id,HttpServletRequest request){
		
		request.setAttribute("chart", csvdataService.findCsvdataById(Integer.valueOf(id)));
	
		return "/editChart";
	}
	
	@RequestMapping("/toAddChart")
	public String toAddChart(){
		return "/addChart";
	}
	@RequestMapping("/addChart")
	public String addChart(Csvdata chart,HttpServletRequest request){
		chart.setUserid(new User(1));
		chart.setRdate(new Date());
		chart.setButton(0);
		chart.setRtime(new Date());
		csvdataService.save(chart);
		return "redirect:/list.html";
	}
	
	@RequestMapping("/delChart")
	public void delChart(String id,HttpServletResponse response){
		String result = "{\"result\":\"error\"}";
		csvdataService.delCsvdata(Long.valueOf(id));
			result = "{\"result\":\"success\"}";
		
		
		response.setContentType("application/json");
		
		try {
			PrintWriter out = response.getWriter();
			out.write(result);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@RequestMapping("/updateChart")
	public String updateChart(Csvdata chart,HttpServletRequest request){
		Csvdata sss = csvdataService.findCsvdataById(Integer.valueOf(chart.getId()));
			sss.setActivity(chart.getActivity());
			sss.setTemp(chart.getTemp());
			csvdataService.updateCsvdata(sss);
			
			request.setAttribute("chart", chart);
			return "redirect:/list.html";
	}
	public static void bufferwriterToFile(String str,String path) {
		BufferedWriter bw = null;
		try {
			bw = new BufferedWriter(new FileWriter(path));
			bw.write(str);
			bw.flush();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				bw.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	public SimpleDateFormat getS() {
		return s;
	}
	public void setS(SimpleDateFormat s) {
		this.s = s;
	}
	public CsvdataService getCsvdataService() {
		return csvdataService;
	}
	public void setCsvdataService(CsvdataService csvdataService) {
		this.csvdataService = csvdataService;
	}
	public String getTemprect() {
		return temprect;
	}
	public void setTemprect(String temprect) {
		this.temprect = temprect;
	}
	
	
}
