
package com.csvimport.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.csvimport.model.ScoreData;
import com.csvimport.model.User;
import com.csvimport.service.ScoreDataService;


@Controller
@SessionAttributes("user")
//@Configuration
//@ComponentScan("com.csvimport.service")
public class ScoreController {

    @Autowired
    private ScoreDataService scoreDataService;

    @RequestMapping(value = "/achievements", method = RequestMethod.GET)
    public String signup(Model model, HttpSession session) {
    	User user = (User)session.getAttribute("user");
    	
    	List<ScoreData> scoreData=scoreDataService.findByUser(user);
    	model.addAttribute("scoredata",scoreData);
    	model.addAttribute("name", user.getFirstname().toString());
        return "achievements";
    }
    
     public void setScoreDataService(ScoreDataService scoreDataServic) {
        this.scoreDataService = scoreDataServic;
    }
     

}
