
package com.csvimport.controller;

import javax.validation.Valid;
import com.csvimport.model.User;
import com.csvimport.model.UserLogin;
import com.csvimport.service.UserService;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

@Controller
@SessionAttributes("user")
public class UserController {

    @Autowired
    private UserService userService;

    @RequestMapping(value = "/signup", method = RequestMethod.GET)
    public String signup(Model model) {
        User user = new User();
        model.addAttribute("user", user);
        return "signup";
    }
    

    //Adding statistics view
//    @RequestMapping(value = "/stats", method = RequestMethod.GET)
//    public String stats(Model model) {
//        User user = new User();
//        model.addAttribute("stats", user);
//        return "stats";
//    }

//    @RequestMapping(value = "/login", method = RequestMethod.GET)
//    public String login(Model model) {
//        UserLogin userLogin = new UserLogin();
//        model.addAttribute("userLogin", userLogin);
//        return "login";
//    }

//=======
//>>>>>>> 21866357a52837140bcf9085c80a68f9767b269a
    @RequestMapping(value = "/signup", method = RequestMethod.POST)
    public String signup(@Valid @ModelAttribute("user") User user, BindingResult result, Model model) {
        if (result.hasErrors()) {
            return "signup";
        } else if (userService.findByUserName(user.getUserid())) {
            model.addAttribute("message", "User Name exists. Try another user name");
            return "signup";
        } else {
            userService.save(user);
            model.addAttribute("message", "Saved userdetails");
            return "redirect:login.html";
        }
    }
    
     @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String login(Model model) {
        UserLogin userLogin = new UserLogin();
        model.addAttribute("userLogin", userLogin);
        return "login";
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public String login(@Valid @ModelAttribute("userLogin") UserLogin userLogin, BindingResult result, HttpSession session, Model model) {
        if (result.hasErrors()) {
            return "login";
        } else {
            boolean found = userService.findByLogin(userLogin.getUserName(), userLogin.getPassword());
            if (found) {
                User user = userService.findByUser(userLogin.getUserName());
                session.setAttribute("user", user);
                model.addAttribute("userName", user.getUserid());
                return "success";
            } else {
                session.setAttribute("user", null);
                return "failure";
            }
        }

    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

}
