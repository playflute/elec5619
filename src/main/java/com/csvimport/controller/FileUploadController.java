
package com.csvimport.controller;

import com.csvimport.model.Csvdata;
import com.csvimport.model.FileUpload;
import com.csvimport.model.User;
import com.csvimport.service.CsvdataService;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;

import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class FileUploadController {

    @Autowired
    private CsvdataService csvdataService;

    @RequestMapping(value = "/upload", method = RequestMethod.GET)
    public String upload(Model model) {
        FileUpload fileUpload = new FileUpload();
        model.addAttribute("fileUpload", fileUpload);
        return "upload";
    }

    @RequestMapping(value = "/upload", method = RequestMethod.POST)
    public String upload(@Valid @ModelAttribute("fileUpload") FileUpload fileUpload, BindingResult result, Model model, HttpSession session) {
        if (!fileUpload.getFile().isEmpty()) {
            try {

                BufferedReader reader = new BufferedReader(new InputStreamReader(fileUpload.getFile().getInputStream()));
                String strTemp[] = null;
                Csvdata csvdata = null;
                String strDate = "";
                String strTime = "";
                Date rdate = null;
                Date rtime = null;
                for (String line; (line = reader.readLine()) != null;) {
                    strTemp = line.split(",");
                    if (strTemp.length >= 11) {
                        try {
                            csvdata = new Csvdata();
                            strDate = strTemp[0].substring(0, 10);
                            strTime = strTemp[0].substring(10, strTemp[0].length());
                            rdate = new SimpleDateFormat("yyyy-MM-dd").parse(strDate);
                            rtime = new SimpleDateFormat("hh:mm:ss:SSS").parse(strTime);

                            csvdata.setRdate(rdate);
                            csvdata.setRtime(rtime);
                            csvdata.setXAxisMean(Double.parseDouble(strTemp[1]));
                            csvdata.setYAxisMean(Double.parseDouble(strTemp[2]));
                            csvdata.setZAxisMean(Double.parseDouble(strTemp[3]));
                            csvdata.setLux(Integer.parseInt(strTemp[4]));
                            csvdata.setButton(Integer.parseInt(strTemp[5]));
                            csvdata.setTemp(Double.parseDouble(strTemp[6]));
                            csvdata.setSvmgs(Integer.parseInt(strTemp[7].substring(0, strTemp[7].indexOf("."))));
                            csvdata.setXAxisStdev(Integer.parseInt(strTemp[8].substring(0, strTemp[8].indexOf("."))));
                            csvdata.setYAxisStdev(Integer.parseInt(strTemp[9].substring(0, strTemp[9].indexOf("."))));
                            csvdata.setZAxisStdev(Integer.parseInt(strTemp[10].substring(0, strTemp[10].indexOf("."))));
                            csvdata.setLight(Integer.parseInt(strTemp[11]));
                            csvdata.setActivity(Math.random()*10);

                            User user = (User) session.getAttribute("user");
                            csvdata.setUserid(user);
                            csvdataService.save(csvdata);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
                model.addAttribute("fileName", "You successfully uploaded " + fileUpload.getFile().getOriginalFilename());
                return "upload_succ";
            } catch (Exception e) {
                model.addAttribute("You failed to upload " + fileUpload.getFile().getOriginalFilename() + " => " + e.getMessage());
                return "upload";
            }
        } else {
            model.addAttribute("You failed to upload " + fileUpload.getFile().getOriginalFilename() + " because the file was empty.");
            return "upload";
        }

    }

    public void setCsvdataService(CsvdataService csvdataService) {
        this.csvdataService = csvdataService;
    }

}
