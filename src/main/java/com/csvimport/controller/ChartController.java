
package com.csvimport.controller;

import com.csvimport.model.ChartData;
import com.csvimport.model.Csvdata;
import com.csvimport.model.User;
import com.csvimport.service.CsvdataService;
import com.csvimport.service.UserService;
import com.keypoint.PngEncoder;
import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartRenderingInfo;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.CategoryLabelPositions;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.NumberTickUnit;
import org.jfree.chart.entity.StandardEntityCollection;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class ChartController {

    @Autowired
    private CsvdataService csvdataService;
    @Autowired
    ServletContext context;
    @Autowired
    private UserService userService;

    @RequestMapping(value = "/chartdashboard", method = RequestMethod.GET)
    public ModelAndView chart(Model model, HttpSession session, ModelAndView mav) {
        User user = (User) session.getAttribute("user");
        ChartData chartData = new ChartData();
        model.addAttribute("chartData", chartData);
        if (user.getRole().equalsIgnoreCase("user")) {
            mav.setViewName("chartdashboarduser");
            return mav;
        } else {
            List<User> userList = userService.findAll();
            mav.addObject("userList", userList);
            mav.setViewName("chartdashboardresearcher");
            return mav;
        }
    }

    @RequestMapping(value = "/chartdashboard", method = RequestMethod.POST)
    public ModelAndView chart(@Valid @ModelAttribute("chartData") ChartData chartData, BindingResult result, Model model, HttpServletResponse response, HttpSession session, ModelAndView mav) {
        response.setContentType("image/png");
        try {
            User user = (User) session.getAttribute("user");

            if (user.getRole().equalsIgnoreCase("user")) {
                for (int i = 1; i <= 9; i++) {
                    CategoryDataset dataset = createDataset(user, chartData);
                    JFreeChart chart = createChart(dataset, chartData.getChartDate());
                    ChartRenderingInfo info = new ChartRenderingInfo(new StandardEntityCollection());
                    File file1 = new File(context.getRealPath("/") + "/chart" + i + ".png");
                    ChartUtilities.saveChartAsPNG(
                            file1, chart, 600, 150, info);
                    chartData.getChartDate().setDate(chartData.getChartDate().getDate() + 1);
                }
                mav.setViewName("chartdashboarduser");
                return mav;
            } else {
                User user1 = userService.findByUser(chartData.getUserID1());
                User user2 = userService.findByUser(chartData.getUserID2());
                for (int i = 1; i <= 9; i++) {
                    CategoryDataset dataset = createDataset(user1, chartData);
                    JFreeChart chart = createChart(dataset, chartData.getChartDate());
                    ChartRenderingInfo info = new ChartRenderingInfo(new StandardEntityCollection());
                    File file1 = new File(context.getRealPath("/") + "/chart" + i + ".png");
                    ChartUtilities.saveChartAsPNG(
                            file1, chart, 600, 150, info);
                    chartData.getChartDate().setDate(chartData.getChartDate().getDate() + 1);
                }
                chartData.getChartDate().setDate(chartData.getChartDate().getDate() - 9);
                for (int i = 1; i <= 9; i++) {
                    CategoryDataset dataset = createDataset(user2, chartData);
                    JFreeChart chart = createChart(dataset, chartData.getChartDate());
                    ChartRenderingInfo info = new ChartRenderingInfo(new StandardEntityCollection());
                    File file1 = new File(context.getRealPath("/") + "/chart1" + i + ".png");
                    ChartUtilities.saveChartAsPNG(
                            file1, chart, 600, 150, info);
                    chartData.getChartDate().setDate(chartData.getChartDate().getDate() + 1);
                }
                List<User> userList = userService.findAll();
                mav.addObject("userList", userList);
                mav.setViewName("chartdashboardresearcher");
                return mav;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return mav;
        }
    }

    /**
     * Creates a sample dataset.
     *
     * @return The dataset.
     */
    private CategoryDataset createDataset(User user, ChartData chartData) {
        List<Csvdata> csvdataList = csvdataService.findByUser(user, chartData.getChartDate());
        // row keys...
        String series1 = "Temperature";
        String series2 = "Light";
        String series3 = "Activity";
        // create the dataset...
        DefaultCategoryDataset dataset = new DefaultCategoryDataset();
        for (int i = 0; i < csvdataList.size(); i++) {
            dataset.addValue(csvdataList.get(i).getTemp(), series1, csvdataList.get(i).getRtime());
            dataset.addValue(csvdataList.get(i).getLight(), series2, csvdataList.get(i).getRtime());
            dataset.addValue(csvdataList.get(i).getActivity(), series3, csvdataList.get(i).getRtime());
        }
        return dataset;

    }

    private JFreeChart createChart(final CategoryDataset dataset, Date chartDate) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        // create the chart...
        JFreeChart chart = ChartFactory.createLineChart(
                "", // chart title
                "", // domain axis label
                "" + simpleDateFormat.format(chartDate), // range axis label
                dataset, // data
                PlotOrientation.VERTICAL, // orientation
                false, // include legend
                false, // tooltips
                false // urls
        );

        CategoryPlot plot = (CategoryPlot) chart.getPlot();
        // customise the range axis...
        NumberAxis rangeAxis = (NumberAxis) plot.getRangeAxis();
        rangeAxis.setTickUnit(new NumberTickUnit(500));
//        rangeAxis.setLabel("22222");
        rangeAxis.setVisible(true);
        CategoryAxis domainAxis = (CategoryAxis) plot.getDomainAxis();
        domainAxis.setVisible(false);
        return chart;
    }
}
