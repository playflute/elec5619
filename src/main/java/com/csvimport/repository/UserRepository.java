package com.csvimport.repository;

import com.csvimport.model.User;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository("userRepository")
public interface UserRepository extends JpaRepository<User, Long> {

    @Query("SELECT u FROM User u WHERE u.userid = :userid")
    User findByUserName(@Param("userid") String userName);

    @Query("SELECT u FROM User u")
    List<User> findAll();

}
