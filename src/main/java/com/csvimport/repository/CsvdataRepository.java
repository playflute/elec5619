
package com.csvimport.repository;

import com.csvimport.model.Csvdata;
import com.csvimport.model.User;
import java.util.Date;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository("csvdataRepository")
public interface CsvdataRepository extends JpaRepository<Csvdata, Long> {

    @Query("SELECT c FROM Csvdata c WHERE c.userid = :userid and c.rdate = :rdate")
    List<Csvdata> findByUser(@Param("userid") User user,@Param("rdate") Date rdate);
    @Query("SELECT c FROM Csvdata c WHERE c.id = :id")
    Csvdata findCsv(@Param("id") Integer id);
}
