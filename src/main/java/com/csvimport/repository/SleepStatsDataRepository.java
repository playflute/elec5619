package com.csvimport.repository;

import java.util.List;

import com.csvimport.model.SleepStatsData;
import com.csvimport.model.User;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository("SleepStatsDataRepository")
public interface SleepStatsDataRepository extends JpaRepository<SleepStatsData, Long> {
	
	@Query("SELECT u FROM SleepStatsData u WHERE u.userid = :userid")
	SleepStatsData findById(@Param("userid") int userid);
	
	@Query("SELECT u FROM SleepStatsData u WHERE u.userid = :userid  order by date")
	List<SleepStatsData> findByUser(@Param("userid") User user);
	
}
