package com.csvimport.repository;

import java.util.List;

import com.csvimport.model.ScoreData;
import com.csvimport.model.SleepStatsData;
import com.csvimport.model.User;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository("ScoreDataRepository")
public interface ScoreDataRepository extends JpaRepository<ScoreData, Long> {
	
	@Query("SELECT u FROM ScoreData u WHERE u.userid = :userid")
	ScoreData findById(@Param("userid") int userid);
	
	@Query("SELECT u FROM ScoreData u WHERE u.userid = :userid  order by date")
	List<ScoreData> findByUser(@Param("userid") User user);
	
}
