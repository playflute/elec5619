
package com.csvimport.validator;

import com.csvimport.model.FileUpload;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

 
public class FileUploadValidator implements Validator{
 
//<<<<<<< HEAD
//
//=======
//>>>>>>> 21866357a52837140bcf9085c80a68f9767b269a
	public boolean supports(Class clazz) {
		//just validate the FileUpload instances
		return FileUpload.class.isAssignableFrom(clazz);
	}
 
	public void validate(Object target, Errors errors) {
 
		FileUpload file = (FileUpload)target;
 
		if(file.getFile().getSize()==0){
			errors.rejectValue("file", "required.fileUpload");
		}
	}
}