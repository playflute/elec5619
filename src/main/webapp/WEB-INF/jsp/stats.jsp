<%@ include file="/WEB-INF/jsp/include.jsp" %>

<html>
  <head><title><fmt:message key="stats.title"/> </title>
  
  
  		<link href="bootstrap/css/bootstrap.css" rel="stylesheet" />
        <link href="datepicker/css/datepicker.css" rel="stylesheet" />
        <link href="assets/css/bootstrap-united.css" rel="stylesheet" />

        <style>
            .green {
                font-weight: bold;
                color: green;
            }

            .message {
                margin-bottom: 10px;
            }

            .error {
                color: #ff0000;
                font-size: 0.9em;
                font-weight: bold;
            }

            .errorblock {
                color: #000;
                background-color: #ffEEEE;
                border: 3px solid #ff0000;
                padding: 8px;
                margin: 16px;
            }
        </style>
  
  
 	<!-- GOGLE CHART API -->
	<script type="text/javascript" src="https://www.google.com/jsapi"></script>
	<script type="text/javascript">
		google.load("visualization", "1", {packages:["corechart"]});
		google.setOnLoadCallback(drawChart);
	
		function drawChart() {
	
		  var data = google.visualization.arrayToDataTable([
		    ['Day', 'Hours Slept'],
		    <c:forEach items="${sleepstats}" var="ss">
		    [<c:out value="${ss.date}"/>, <c:out value="${ss.sleepDuration}"/> ], 
	    	</c:forEach>
		  ]);
	
		  var options = {
		    title: 'Sleep Statistics',
		    width: 700,
		    height: 500,
		    fontSize: '20',
		    fontName: 'Times New Roman',
		    hAxis: {title: 'Days', titleTextStyle: {color: 'red', font: 'Arial', fontSize: 20}},
		      vAxis: {title: 'Hours Slept', titleTextStyle: {color: 'red'}, maxValue: 24, minValue: 0}
		  };
	
		  var chart = new google.visualization.ColumnChart(document.getElementById('chart_div'));
	
		  chart.draw(data, options);

	}
	</script>
  
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.17/jquery-ui.min.js"></script>
  <link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.17/themes/pepper-grinder/jquery-ui.css"/>
  </head>
  <body>
  
  
  <div class="navbar navbar-default">

            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse"
                        data-target=".navbar-responsive-collapse">
                    <span class="icon-bar"></span> <span class="icon-bar"></span> <span
                        class="icon-bar"></span>
                </button>
            </div>

            <div class="navbar-collapse collapse navbar-responsive-collapse">

                <ul class="nav navbar-nav navbar-right">
                	<li><a href="upload.html">Sensory Data</a></li>
                    <li><a href="list.html">Sensor Visualisations</a></li>
                    <li><a href="achievements.html">Scores</a></li>
                    <li><a href="login.html">Logout</a></li>
                </ul>
            </div>
            <!-- /.nav-collapse -->
        </div>

        <script src="jquery-1.8.3.js">

        </script>

        <script src="bootstrap/js/bootstrap.js">

        </script>

        <script src="datepicker/js/bootstrap-datepicker.js">

        </script>
  
  
  
  
  
  <div class="container">
  	<div class="jumbotron">
    	<h1> Sleep Statistics</h1>
    	<h2><c:out value="${name}"/></h2>
    	<br />
    	<label>Total Sleep duration in Past 7 days: <c:out value="${totalSleep}"/></label><br />
    	<label>Maximum Sleep Duration: <c:out value="${maxSleep}"/></label><br />
    	<label>Minimum Sleep Duration: <c:out value="${minSleep}"/></label><br />
  	</div>
  
 <%--  <div>
  	<c:forEach items="${sleepstats}" var="ss">
		Sleep Duration: <c:out value="${ss.sleepDuration}"/>  <br>
		Date: <c:out value="${ss.date}"/> <br>
		<br>
    </c:forEach>
    <br>
  	
  </div> --%>
  
  
<!--     <form>
    	<label>From: </label>
    	<input  id="startDate" type="text" name="date"/>
    	<script>
			$(function() {
				$( "#startDate" ).datepicker({ });
			});
		</script>
    	<label>To: </label>
    	<input id="endDate" type="text" name="date"/>
    	<script>
			$(function() {
				$( "#endDate" ).datepicker({ });
			});
		</script>
    </form>
    <script>
	$(function() {
		$( "#datepicker" ).datepicker({ });
	});
	</script> -->
    
    <div id="chart_div" style="width: 900px; height: 500px; font: 'Arial'"></div>
	
	<!-- FACEBOOK SDK -->
    <script src="datepicker/js/facebookscript.js"></script>
	<div id="fb-root"></div>
	<div class="fb-share-button" data-href="https://developers.facebook.com/docs/plugins/"></div>
    
  </div>  
    <footer>Time on server is: <c:out value="${now}"/></footer>
  </body>
</html>