<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ include file="/WEB-INF/jsp/include.jsp" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>


<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Login Success</title>
        

		<link href="assets/css/bootstrap-united.css" rel="stylesheet" />
        <!-- Custom CSS -->
        <link href="assets/css/simple-sidebar.css" rel="stylesheet">
        

<!-- <script type="text/javascript" src="/UploadCSVWithSpring/resources/js/jquery-1.7.1.js"></script> -->
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<script type="text/javascript">
	function del(id){
		$.get("delChart.html?id=" + id,function(data){
			if("success" == data.result){
				alert("delete success");
				window.location.reload();
			}else{
				alert("delete failure");
			}
		});
	}
</script>
</head>
<body>

		<script src="jquery-1.8.3.js">

        </script>

        <script src="bootstrap/js/bootstrap.js">

        </script>

        <div class="navbar navbar-default">

            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse"
                        data-target=".navbar-responsive-collapse">
                    <span class="icon-bar"></span> <span class="icon-bar"></span> <span
                        class="icon-bar"></span>
                </button>
            </div>

            <div class="navbar-collapse collapse navbar-responsive-collapse">
                <ul class="nav navbar-nav navbar-right">
                	<li><a href="upload.html">Sensory Data</a></li>
                    <li><a href="list.html">Sensor Visualisations</a></li>
                    <li><a href="stats.html">Statistics</a></li>
                    <li><a href="achievements.html">Scores</a></li>
                    <li><a href="login.html">Logout</a></li>
                </ul>
            </div>
            <!-- /.nav-collapse -->
        </div>
        

<center>
	<h6><a href="toAddChart.html">add terperature</a></h6>
	<table border="1" style="color: fuchsia">
		<tbody>
			<tr>
				<th>temperature</th>
				<th>activity</th>
				<th>datetime</th>
				<th>operation</th>
			</tr>
			<c:if test="${!empty chartList }">
				<c:forEach items="${chartList }" var="chart">
					<tr>
						<td>${chart.temp }</td>
						<td>${chart.activity }</td>
						<td>${chart.rdate} ${chart.rtime} </td>
						<td>
							<a href="getChart.html?id=${chart.id }">edit</a>
						</td>
					</tr>				
				</c:forEach>
			</c:if>
			<a href="getAllChart.html">view chart</a>
		</tbody>
	</table>
	</center>
</body>
</html>