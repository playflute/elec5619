<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <link href="assets/css/bootstrap-united.css" rel="stylesheet" />
        <link href="datepicker/css/datepicker.css" rel="stylesheet" />
        <!-- Custom CSS -->
        <link href="assets/css/simple-sidebar.css" rel="stylesheet">
        <style>
            .error {
                color: #ff0000;
                font-size: 0.9em;
                font-weight: bold;
            }

            .errorblock {
                color: #000;
                background-color: #ffEEEE;
                border: 3px solid #ff0000;
                padding: 8px;
                margin: 16px;
            }
        </style>
        <title>File Upload</title>
    </head>
    <body>
        <script src="jquery-1.8.3.js">

        </script>

        <script src="bootstrap/js/bootstrap.js">

        </script>
        <script src="datepicker/js/bootstrap-datepicker.js">

        </script>
        <div class="navbar navbar-default">

            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse"
                        data-target=".navbar-responsive-collapse">
                    <span class="icon-bar"></span> <span class="icon-bar"></span> <span
                        class="icon-bar"></span>
                </button>
            </div>

            <div class="navbar-collapse collapse navbar-responsive-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="list.html">Sensor Visualizations</a></li>
                    <li><a href="stats.html">Statistics</a></li>
                    <li><a href="achievements.html">Scores</a></li>
                    <li><a href="login.html">Logout</a></li>
                </ul>
            </div>
            <!-- /.nav-collapse -->
        </div>

        <div class="col-lg-6 col-lg-offset-3">
            <div class="well">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6">
                            <form:form id="myForm" method="post"
                                       class="bs-example form-horizontal" commandName="chartData">
                                <fieldset>
                                    <legend>Generate Chart</legend>

                                    <div class="form-group">
                                        <label for="dateOfBirthInput" class="col-lg-3 control-label">Select Start Date</label>
                                        <div class="date form_date col-lg-9" data-date-format="mm/dd/yyyy" data-date-viewmode="years">
                                            <form:input type="text" class="form-control"											
                                                        path="chartDate" id="dateOfBirthInput"
                                                        placeholder="Chart Date" />
                                            <form:errors path="chartDate" cssClass="error" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="gendeInput" class="col-lg-3 control-label">Select User1</label>
                                        <div class="col-lg-9">
                                            <form:select class="form-control"
                                                         path="userID1" id="genderInput"
                                                         placeholder="Gender" >
                                                <form:options items="${userList}" itemLabel="userid" itemValue="userid"/>
                                            </form:select>
                                            <form:errors path="userID1" cssClass="error" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="gendeInput" class="col-lg-3 control-label">Select User2</label>
                                        <div class="col-lg-9">
                                            <form:select class="form-control"
                                                         path="userID2" id="genderInput"
                                                         placeholder="Gender" >
                                                <form:options items="${userList}" itemLabel="userid" itemValue="userid"/>
                                            </form:select>
                                            <form:errors path="userID2" cssClass="error" />
                                        </div>
                                    </div>

                                    <div class="col-lg-9 col-lg-offset-3">
                                        <button class="btn btn-primary">Generate Chart</button>
                                    </div>

                                </fieldset>
                            </form:form>
                        </div>
                    </div>
                </div>
            </div>
            <table>
                <tr>
                    <td><IMG SRC="chart1.png" WIDTH="600" HEIGHT="150" BORDER="0" USEMAP="#chart">
                        <br>
                        <IMG SRC="chart2.png" WIDTH="600" HEIGHT="150" BORDER="0" USEMAP="#chart">
                        <br>
                        <IMG SRC="chart3.png" WIDTH="600" HEIGHT="150" BORDER="0" USEMAP="#chart">
                        <br>
                        <IMG SRC="chart4.png" WIDTH="600" HEIGHT="150" BORDER="0" USEMAP="#chart">
                        <br>
                        <IMG SRC="chart5.png" WIDTH="600" HEIGHT="150" BORDER="0" USEMAP="#chart">
                        <br>
                        <IMG SRC="chart6.png" WIDTH="600" HEIGHT="150" BORDER="0" USEMAP="#chart">
                        <br>
                        <IMG SRC="chart7.png" WIDTH="600" HEIGHT="150" BORDER="0" USEMAP="#chart">
                        <br>
                        <IMG SRC="chart8.png" WIDTH="600" HEIGHT="150" BORDER="0" USEMAP="#chart">
                        <br>
                        <IMG SRC="chart9.png" WIDTH="600" HEIGHT="150" BORDER="0" USEMAP="#chart"></td>
                    <td><IMG SRC="chart11.png" WIDTH="600" HEIGHT="150" BORDER="0" USEMAP="#chart">
                        <br>
                        <IMG SRC="chart12.png" WIDTH="600" HEIGHT="150" BORDER="0" USEMAP="#chart">
                        <br>
                        <IMG SRC="chart13.png" WIDTH="600" HEIGHT="150" BORDER="0" USEMAP="#chart">
                        <br>
                        <IMG SRC="chart14.png" WIDTH="600" HEIGHT="150" BORDER="0" USEMAP="#chart">
                        <br>
                        <IMG SRC="chart15.png" WIDTH="600" HEIGHT="150" BORDER="0" USEMAP="#chart">
                        <br>
                        <IMG SRC="chart16.png" WIDTH="600" HEIGHT="150" BORDER="0" USEMAP="#chart">
                        <br>
                        <IMG SRC="chart17.png" WIDTH="600" HEIGHT="150" BORDER="0" USEMAP="#chart">
                        <br>
                        <IMG SRC="chart18.png" WIDTH="600" HEIGHT="150" BORDER="0" USEMAP="#chart">
                        <br>
                        <IMG SRC="chart19.png" WIDTH="600" HEIGHT="150" BORDER="0" USEMAP="#chart"></td>
                </tr>
            </table>
        </div>


        <div id="wrapper">

            <!-- Sidebar -->
            <div id="sidebar-wrapper">
                <ul class="sidebar-nav">
                    <li class="sidebar-brand">
                        <a  href="<spring:url value="upload.html"/>">Upload File</a>
                    </li>
                    <li class="sidebar-brand">
                        <a  href="<spring:url value="chartdashboard.html"/>">Light & Temp Charts</a>
                    </li>
                </ul>
            </div>
            <!-- /#sidebar-wrapper -->

        </div>
        <script>
            $(function () {
                $('#dateOfBirthInput').datepicker();
            });
        </script>
        <!-- /#wrapper -->
    </body>
</html>