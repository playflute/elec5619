<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Login Success</title>
        <link href="assets/css/bootstrap-united.css" rel="stylesheet" />
        <!-- Custom CSS -->
        <link href="assets/css/simple-sidebar.css" rel="stylesheet">
<title>Insert title here</title>
<script language="javascript" type="text/javascript" src="/UploadCSVWithSpring/resources/js/datepicker/WdatePicker.js"></script>
<script type="text/javascript">
	function addChart(){
		var form = document.forms[0];
		form.action = "addChart.html";
		form.method="post";
		form.submit();
	}
</script>
</head>
<body>
 <script src="jquery-1.8.3.js">

        </script>

        <script src="bootstrap/js/bootstrap.js">

        </script>

        <div class="navbar navbar-default">

            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse"
                        data-target=".navbar-responsive-collapse">
                    <span class="icon-bar"></span> <span class="icon-bar"></span> <span
                        class="icon-bar"></span>
                </button>
            </div>

            <div class="navbar-collapse collapse navbar-responsive-collapse">
                <ul class="nav navbar-nav navbar-right">
                	<li><a href="upload.html">Sensory Data</a></li>
                    <li><a href="list.html">Sensor Visualisations</a></li>
                    <li><a href="stats.html">Statistics</a></li>
                    <li><a href="achievements.html">Scores</a></li>
                    <li><a href="login.html">Logout</a></li>
                </ul>
            </div>
	<h1>add temperature</h1>
	<form action="" name="chartForm">
		temperature：<input type="text" name="temp">
		activity:<input type="text" name="activity">
		<input type="button" value="add" onclick="addChart()">
		<input type="button" value="cancel" onclick="javascript:history.go(-1);"/>
	</form>
</body>
</html>