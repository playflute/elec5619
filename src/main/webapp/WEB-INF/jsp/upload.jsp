<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <link href="assets/css/bootstrap-united.css" rel="stylesheet" />
        <!-- Custom CSS -->
        <link href="assets/css/simple-sidebar.css" rel="stylesheet">
        <style>
            .error {
                color: #ff0000;
                font-size: 0.9em;
                font-weight: bold;
            }

            .errorblock {
                color: #000;
                background-color: #ffEEEE;
                border: 3px solid #ff0000;
                padding: 8px;
                margin: 16px;
            }
        </style>
        <title>File Upload</title>
    </head>
    <body>
        <script src="jquery-1.8.3.js">

        </script>

        <script src="bootstrap/js/bootstrap.js">

        </script>

        <div class="navbar navbar-default">

            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse"
                        data-target=".navbar-responsive-collapse">
                    <span class="icon-bar"></span> <span class="icon-bar"></span> <span
                        class="icon-bar"></span>
                </button>
            </div>

            <div class="navbar-collapse collapse navbar-responsive-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="list.html">Sensor Visualisations</a></li>
                    <li><a href="stats.html">Statistics</a></li>
                    <li><a href="achievements.html">Scores</a></li>
                    <li><a href="login.html">Logout</a></li>
                </ul>
            </div>
            <!-- /.nav-collapse -->
        </div>

        <div class="col-lg-6 col-lg-offset-3">
            <div class="well">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6">
                            <form:form id="myForm" method="post" enctype="multipart/form-data"
                                       class="bs-example form-horizontal" commandName="fileUpload">
                                <fieldset>
                                    <legend>File Upload Form</legend>

                                    <div class="form-group">
                                        <label for="fileUploadInput" class="col-lg-3 control-label">File to upload</label>
                                        <div class="col-lg-9">
                                            <input type="file" name="file" id="fileUploadInput"/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="descriptionInput" class="col-lg-3 control-label">File Description</label>
                                        <div class="col-lg-9">
                                            <form:input type="text" class="form-control" path="description"
                                                        id="descriptionInput" placeholder="User Name" />
                                            <form:errors path="description" cssClass="error" />
                                        </div>
                                    </div>

                                    <div class="col-lg-9 col-lg-offset-3">
                                        <button class="btn btn-default">Cancel</button>

                                        <button class="btn btn-primary">Upload</button>
                                    </div>

                                </fieldset>
                            </form:form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="wrapper">

            <!-- Sidebar -->
            <div id="sidebar-wrapper">
                <ul class="sidebar-nav">
                    <li class="sidebar-brand">
                        <a  href="<spring:url value="upload.html"/>">Upload File</a>
                    </li>
                    <li class="sidebar-brand">
                        <a  href="<spring:url value="chartdashboard.html"/>">Light & Temp Charts</a>
                    </li>
                </ul>
            </div>
            <!-- /#sidebar-wrapper -->
            
        </div>
        <!-- /#wrapper -->
    </body>
</html>