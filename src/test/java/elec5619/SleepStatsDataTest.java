package elec5619;

import static org.junit.Assert.*;

import java.util.Date;

import org.junit.Before;
import org.junit.Test;

import com.csvimport.model.SleepStatsData;
import com.csvimport.model.User;

public class SleepStatsDataTest {

	private SleepStatsData sd;
	@Before
	public void setUp() throws Exception {
		sd=new SleepStatsData();
	}

	@Test
	public void testUserId() {
		assertNull(sd.getUserid());
		User u=new User();
		u.setUserid("1");
		u.setFirstname("");
		sd.setUserid(u);
		assertEquals(u.getFirstname(), sd.getUserid().getFirstname());
	}
	
	@Test
	public void testUser() {
		assertNull(sd.getUserid());
		User u=new User();
		u.setUserid("1");
		u.setFirstname("");
		sd.setUserid(u);
		assertEquals(u, sd.getUserid());
	}
	
	@Test
	public void testDate() {
		assertNull(sd.getDate());
		Date d=new Date();
		sd.setDate(d);
		assertEquals(d, sd.getDate());
	}
	
	@Test
	public void testSleepDuration() {
		assertNull(sd.getDate());
		int sleepDuration=10;
		sd.setSleepDuration(sleepDuration);
		assertEquals(sleepDuration, sd.getSleepDuration(),0);
	}

}
