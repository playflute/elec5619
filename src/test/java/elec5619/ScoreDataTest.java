package elec5619;

import static org.junit.Assert.*;

import java.util.Date;

import org.junit.Before;
import org.junit.Test;

import com.csvimport.model.ScoreData;
import com.csvimport.model.User;

public class ScoreDataTest {

	private ScoreData sd;
	@Before
	public void setUp() throws Exception {
		sd=new ScoreData();
	}

	@Test
	public void testUserId() {
		assertNull(sd.getUserid());
		User u=new User();
		u.setUserid("1");
		u.setFirstname("");
		sd.setUserid(u);
		assertEquals(u.getFirstname(), sd.getUserid().getFirstname());
	}
	
	@Test
	public void testUser() {
		assertNull(sd.getUserid());
		User u=new User();
		u.setUserid("1");
		u.setFirstname("");
		sd.setUserid(u);
		assertEquals(u, sd.getUserid());
	}
	
	@Test
	public void testDate() {
		assertNull(sd.getDate());
		Date d=new Date();
		sd.setDate(d);
		assertEquals(d, sd.getDate());
	}
	
	@Test
	public void testConis() {
		assertNull(sd.getDate());
		int coins=10;
		sd.setCoins(coins);
		assertEquals(coins, sd.getCoins());
	}
	
	@Test
	public void testMedal() {
		assertNull(sd.getDate());
		String medal="Gold";
		sd.setMedal(medal);
		assertEquals(medal, sd.getMedal());
	}

}
