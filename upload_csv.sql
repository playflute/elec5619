
SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for csvdata
-- ----------------------------
CREATE TABLE `csvdata` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rdate` date DEFAULT NULL,
  `rtime` time DEFAULT NULL,
  `x_axis_mean` double DEFAULT NULL,
  `y_axis_mean` double DEFAULT NULL,
  `z_axis_mean` double DEFAULT NULL,
  `lux` int(11) DEFAULT NULL,
  `button` int(1) DEFAULT NULL,
  `temp` double(11,0) DEFAULT NULL,
  `svmgs` int(11) DEFAULT NULL,
  `x_axis_stdev` int(11) DEFAULT NULL,
  `y_axis_stdev` int(11) DEFAULT NULL,
  `z_axis_stdev` int(11) DEFAULT NULL,
  `light` int(11) DEFAULT NULL,
  `activity` double DEFAULT NULL,
  `userid` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_id_uderid` (`userid`),
  CONSTRAINT `fk_id_uderid` FOREIGN KEY (`userid`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=13128 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for user
-- ----------------------------
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(100) NOT NULL,
  `userid` varchar(100) NOT NULL,
  `firstname` varchar(100) NOT NULL,
  `lastname` varchar(100) NOT NULL,
  `dateOfBirth` date NOT NULL,
  `emailAddress` varchar(50) DEFAULT NULL,
  `gender` varchar(25) NOT NULL,
  `role` varchar(25) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records 
-- ----------------------------
INSERT INTO `user` VALUES ('1', 'Leo237', 'Leo', 'Leo', 'King', '2014-10-08', 'leo237@gmail.com', 'male', 'user');

