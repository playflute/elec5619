# Sleep Analysis Project #
Details in [wiki](https://bitbucket.org/playflute/elec5619/wiki/Home) page.

This is a project for the course ELEC5619 Object-Oriented Application Frameworks, The University of Sydney.

The project aims to promote healthy sleep by graphing data about sleep periods (collected in csv format via Actigraphy watches), and encourage social activities on facebook regarding these collections.

### Setup ###
- Language(s): This project is coded in java, with jsp for web parts.
- IDE: eclipse
- Framework: Spring MVC (Spring STS plugin for eclipse)
- Server: Tomcat
- Database server: any, e.g. MySQL
- Version control: Git
- Others: Maven - for building project (not really necessary), and is a wonderful help on unit tests.

### Run ###
From eclipse, right click on project -> Run As -> Run on Server (i.e. tomcat.)

### Test ###
From eclipse, right click on project -> Run As -> Maven Test

----
# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact